# neofetch initialization hook
#
# You can use the following variables in this file:
# * $package       package name
# * $path          package path
# * $dependencies  package dependencies

# according to: https://github.com/fish-shell/fish-shell/issues/1774
if type -q neofetch
    set -gx NEOFETCH_VERSION (neofetch --version)
    neofetch
else
     echo "Please install neofetch first. See https://github.com/dylanaraps/neofetch"
     exit 1
end